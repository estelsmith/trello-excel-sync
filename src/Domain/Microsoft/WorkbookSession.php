<?php

namespace App\Domain\Microsoft;

use App\Infrastructure\Microsoft\Excel\SessionManager;
use Microsoft\Graph\Http\GraphCollectionRequest;
use Microsoft\Graph\Http\GraphRequest;
use Microsoft\Graph\Model\WorkbookSessionInfo;

class WorkbookSession
{
    public function __construct(
        private WorkbookSessionInfo $graphSession,
        private SessionManager $sessionManager,
        private string $driveId,
        private string $driveItemId
    )
    {
    }

    public function getGraphSession(): WorkbookSessionInfo
    {
        return $this->graphSession;
    }

    public function getSessionManager(): SessionManager
    {
        return $this->sessionManager;
    }

    public function getDriveId(): string
    {
        return $this->driveId;
    }

    public function getDriveItemId(): string
    {
        return $this->driveItemId;
    }

    public function attachToRequest(GraphRequest $request)
    {
        $request->addHeaders([
            'workbook-session-id' => $this->graphSession->getId()
        ]);
    }

    public function close()
    {
        $this->sessionManager->close($this);
    }
}
