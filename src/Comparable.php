<?php

namespace App;

interface Comparable
{
    public function isSameAs(mixed $compareTo): bool;
    public function isEqualTo(mixed $compareTo): bool;
}
