<?php

namespace App\Model\Trello;

class CardReference
{
    public function __construct(
        private string $shortLink,
        private string $url
    )
    {
    }

    public function getShortLink(): string
    {
        return $this->shortLink;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
