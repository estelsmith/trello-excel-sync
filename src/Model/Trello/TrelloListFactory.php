<?php

namespace App\Model\Trello;

class TrelloListFactory
{
    public function __construct(
        private CardFactory $cardFactory
    )
    {
    }

    public function fromApiListResponse(array $item)
    {
        $cards = null;
        if (array_key_exists('cards', $item)) {
            $cards = [];
            foreach ($item['cards'] as $card) {
                $cards[] = $this->cardFactory->fromCardListItem($card);
            }
        }

        return new TrelloList(
            $item['id'],
            $item['name'],
            $item['idBoard'],
            $item['pos'],
            $cards
        );
    }
}
