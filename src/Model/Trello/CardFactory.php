<?php

namespace App\Model\Trello;

class CardFactory
{
    public function __construct(
        private LabelFactory $labelFactory
    )
    {
    }

    public function fromCardListItem(array $item): Card
    {
        return new Card(
            $item['id'],
            $item['shortLink'],
            $item['shortUrl'],
            $item['name'],
            $item['pos'],
            array_map(
                fn($label) => $this->labelFactory->fromApiResponse($label),
                $item['labels']
            )
        );
    }
}
