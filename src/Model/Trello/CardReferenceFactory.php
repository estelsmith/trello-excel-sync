<?php

namespace App\Model\Trello;

class CardReferenceFactory
{
    public function fromUrl(string $url)
    {
        $shortLink = '';
        $path = parse_url($url, PHP_URL_PATH);
        if (str_starts_with($path, '/c/')) {
            $shortLink = preg_replace('/\/c\/([a-zA-Z0-9]+)\/?.*/', '${1}', $path);
        }

        return new CardReference(
            $shortLink,
            $url
        );
    }
}
