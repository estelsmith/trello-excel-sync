<?php

namespace App\Model\Trello;

class LabelFactory
{
    private $labels = [];

    public function fromApiResponse(array $item): Label
    {
        $labels = &$this->labels;
        if (!array_key_exists($item['id'], $labels)) {
            $labels[$item['id']] = new Label(
                $item['id'],
                $item['idBoard'],
                $item['name'],
                $item['color']
            );
        }

        return $labels[$item['id']];
    }
}
