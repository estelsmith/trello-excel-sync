<?php

namespace App\Model\Trello;

class BoardFactory
{
    public function fromApiResponse(array $item)
    {
        return new Board(
            $item['id'],
            $item['name'],
            $item['shortLink']
        );
    }
}
