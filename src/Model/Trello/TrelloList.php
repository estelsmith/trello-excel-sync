<?php

namespace App\Model\Trello;

class TrelloList
{
    public function __construct(
        private string $id,
        private string $name,
        private string $boardId,
        private float $position,
        private ?iterable $cards = null
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    public function getPosition(): float
    {
        return $this->position;
    }

    public function hasCards(): bool
    {
        return !is_null($this->cards);
    }

    /**
     * @return iterable<Card>|null
     */
    public function getCards(): ?iterable
    {
        return $this->cards;
    }
}
