<?php

namespace App\Model\Trello;

class Board
{
    public function __construct(
        private string $id,
        private string $name,
        private string $shortLink
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShortLink(): string
    {
        return $this->shortLink;
    }
}
