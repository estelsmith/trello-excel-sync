<?php

namespace App\Model\Trello;

class Label
{
    public function __construct(
        private string $id,
        private string $boardId,
        private string $name,
        private string $color
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): string
    {
        return $this->color;
    }
}
