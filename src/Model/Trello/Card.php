<?php

namespace App\Model\Trello;

class Card
{
    /**
     * @param string $id
     * @param string $shortLink
     * @param string $shortUrl
     * @param string $name
     * @param float $position
     * @param iterable<Label> $labels
     */
    public function __construct(
        private string $id,
        private string $shortLink,
        private string $shortUrl,
        private string $name,
        private float $position,
        private iterable $labels = []
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getShortLink(): string
    {
        return $this->shortLink;
    }

    public function getShortUrl(): string
    {
        return $this->shortUrl;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPosition(): float
    {
        return $this->position;
    }

    /**
     * @return iterable<Label>
     */
    public function getLabels()
    {
        return $this->labels;
    }
}
