<?php

namespace App\Model\Worksheet;

use App\Model\Trello\CardReference;

class Task
{
    public function __construct(
        private string $board,
        private string $title,
        private string $comments,
        private string $projectInitiative,
        private string $estimatedStart,
        private string $weeksToComplete,
        private string $size,
        private string $priority,
        private CardReference $trelloCard
    )
    {
    }

    public function getBoard(): string
    {
        return $this->board;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getComments(): string
    {
        return $this->comments;
    }

    public function getProjectInitiative(): string
    {
        return $this->projectInitiative;
    }

    public function getEstimatedStart(): string
    {
        return $this->estimatedStart;
    }

    public function getWeeksToComplete(): string
    {
        return $this->weeksToComplete;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function getPriority(): string
    {
        return $this->priority;
    }

    public function getTrelloCard(): CardReference
    {
        return $this->trelloCard;
    }
}
