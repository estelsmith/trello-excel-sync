<?php

namespace App\Model\Worksheet;

use App\Model\Trello\CardReferenceFactory;

class TaskFactory
{
    public function __construct(
        private CardReferenceFactory $cardReferenceFactory
    )
    {
    }

    public function fromWorksheetRow(array $row)
    {
        return new Task(
            $row['board/category'],
            $row['title'],
            $row['comments'],
            $row['project initiative'],
            $row['estimated start'],
            $row['weeks to complete'],
            $row['size'],
            $row['priority'],
            $this->cardReferenceFactory->fromUrl($row['trello card'])
        );
    }
}
