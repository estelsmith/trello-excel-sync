<?php

namespace App\Model\App\Excel;

class WorksheetReference
{
    public function __construct(
        private string $driveId,
        private string $driveItemId,
        private string $worksheetId
    )
    {
    }

    public function getDriveId(): string
    {
        return $this->driveId;
    }

    public function getDriveItemId(): string
    {
        return $this->driveItemId;
    }

    public function getWorksheetId(): string
    {
        return $this->worksheetId;
    }
}
