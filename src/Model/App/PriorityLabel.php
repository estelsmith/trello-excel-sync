<?php

namespace App\Model\App;

class PriorityLabel
{
    public function __construct(
        private string $priority,
        private string $name
    )
    {
    }

    public function getPriority(): string
    {
        return $this->priority;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
