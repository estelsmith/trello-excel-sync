<?php

namespace App\Command\Microsoft\Excel;

use App\CommandInterface;
use App\Model\App\Excel\WorksheetReference;

class InsertRangeCommand implements CommandInterface
{
    public const SHIFT_RIGHT = 'Right';
    public const SHIFT_DOWN = 'Down';

    public function __construct(
        private WorksheetReference $worksheetReference,
        private string $address,
        private string $shift = self::SHIFT_DOWN
    )
    {
    }

    public function getWorksheetReference(): WorksheetReference
    {
        return $this->worksheetReference;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getShift(): string
    {
        return $this->shift;
    }
}
