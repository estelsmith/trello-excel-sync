<?php

namespace App\Command\Trello\Label;

use App\CommandInterface;

class CreateLabelCommand implements CommandInterface
{
    public function __construct(
        private string $boardId,
        private string $name,
        private string $color
    )
    {
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): string
    {
        return $this->color;
    }
}
