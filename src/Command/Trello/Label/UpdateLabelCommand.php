<?php

namespace App\Command\Trello\Label;

use App\CommandInterface;

class UpdateLabelCommand implements CommandInterface
{
    public function __construct(
        private string $id,
        private string $name,
        private string $color
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getColor(): string
    {
        return $this->color;
    }
}
