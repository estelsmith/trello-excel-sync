<?php

namespace App\Command\Trello\Card;

use App\CommandInterface;

class SetLabelsCommand implements CommandInterface
{
    public function __construct(
        private string $id,
        private array $labels
    )
    {
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getLabels(): array
    {
        return $this->labels;
    }
}
