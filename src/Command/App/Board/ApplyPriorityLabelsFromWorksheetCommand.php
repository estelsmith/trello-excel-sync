<?php

namespace App\Command\App\Board;

use App\CommandInterface;
use App\Model\App\Excel\WorksheetReference;
use App\Model\App\PriorityLabel;

class ApplyPriorityLabelsFromWorksheetCommand implements CommandInterface
{
    /**
     * @param WorksheetReference $worksheetReference
     * @param string $boardId
     * @param PriorityLabel[] $priorityLabels
     * @param string[] $excludeLists
     */
    public function __construct(
        private WorksheetReference $worksheetReference,
        private string $boardId,
        private iterable $priorityLabels,
        private iterable $excludeLists = ['Recently Released', 'Complete']
    )
    {
    }

    public function getWorksheetReference(): WorksheetReference
    {
        return $this->worksheetReference;
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    /**
     * @return PriorityLabel[]
     */
    public function getPriorityLabels(): iterable
    {
        return $this->priorityLabels;
    }

    /**
     * @return string[]
     */
    public function getExcludeLists(): iterable
    {
        return $this->excludeLists;
    }
}
