<?php

namespace App\Command\App\Board;

use App\CommandInterface;

class SortCardsByPriorityCommand implements CommandInterface
{
    public const DIRECTION_ASCENDING = 1;
    public const DIRECTION_DESCENDING = -1;

    /**
     * SortCardsByPriorityCommand constructor.
     * @param string $boardId
     * @param string[] $labels
     * @param int $direction
     * @param string[] $excludeLists
     */
    public function __construct(
        private string $boardId,
        private iterable $labels,
        private int $direction = self::DIRECTION_ASCENDING,
        private iterable $excludeLists = ['Recently Released', 'Complete']
    )
    {
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    /**
     * @return string[]
     */
    public function getLabels(): iterable
    {
        return $this->labels;
    }

    /**
     * @return int
     */
    public function getDirection(): int
    {
        return $this->direction;
    }

    /**
     * @return string[]
     */
    public function getExcludeLists(): iterable
    {
        return $this->excludeLists;
    }
}
