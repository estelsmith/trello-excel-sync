<?php

namespace App\Command\App;

use App\CommandInterface;
use App\Model\Trello\WantedLabel;

class SyncLabelsToBoardCommand implements CommandInterface
{
    /**
     * @param string $boardId
     * @param iterable<WantedLabel> $wantedLabels
     */
    public function __construct(
        private string $boardId,
        private iterable $wantedLabels
    )
    {
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    /**
     * @return iterable<WantedLabel>
     */
    public function getWantedLabels()
    {
        return $this->wantedLabels;
    }
}
