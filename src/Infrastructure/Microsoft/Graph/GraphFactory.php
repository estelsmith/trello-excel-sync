<?php

namespace App\Infrastructure\Microsoft\Graph;

use App\Authentication\Authenticator\MicrosoftAuthenticator;
use Microsoft\Graph\Graph;

class GraphFactory
{
    public function __construct(
        private MicrosoftAuthenticator $authenticator
    )
    {
    }

    public function create(): Graph
    {
        return (new Graph())
            ->setAccessToken($this->authenticator->getAccessToken());
    }
}
