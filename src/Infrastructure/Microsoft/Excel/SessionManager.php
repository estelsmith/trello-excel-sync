<?php

namespace App\Infrastructure\Microsoft\Excel;

use App\Domain\Microsoft\WorkbookSession;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\WorkbookSessionInfo;

class SessionManager
{
    /**
     * @var array<WorkbookSession>
     */
    private array $sessions = [];

    public function __construct(
        private Graph $graph
    )
    {
    }

    public function __destruct()
    {
        foreach ($this->sessions as $session) {
            $this->close($session);
        }
    }

    public function start(string $driveId, string $driveItemId): WorkbookSession
    {
        $sessions = &$this->sessions;
        $sessionIndex = $this->getItemIndex($driveId, $driveItemId);
        if (array_key_exists($sessionIndex, $sessions)) {
            return $sessions[$sessionIndex];
        }

        $request = $this->graph->createRequest(
            'POST',
            sprintf(
                '/drives/%s/items/%s/workbook/createSession',
                $driveId,
                $driveItemId
            )
        );

        $request->setReturnType(WorkbookSessionInfo::class);

        return $sessions[$sessionIndex] = new WorkbookSession(
            $request->execute(),
            $this,
            $driveId,
            $driveItemId
        );
    }

    public function close(WorkbookSession $session)
    {
        $request = $this->graph->createRequest(
            'POST',
            sprintf(
                '/drives/%s/items/%s/workbook/closeSession',
                $session->getDriveId(),
                $session->getDriveItemId()
            )
        );

        $request->addHeaders([
            'workbook-session-id' => $session->getGraphSession()->getId()
        ]);

        $request->execute();

        $sessionIndex = $this->getItemIndex($session->getDriveId(), $session->getDriveItemId());
        if (array_key_exists($sessionIndex, $this->sessions)) {
            unset($this->sessions[$sessionIndex]);
        }
    }

    private function getItemIndex(string $driveId, string $driveItemId): string
    {
        return sprintf(
            '%s:%s',
            $driveId,
            $driveItemId
        );
    }
}
