<?php

namespace App\Infrastructure\Symfony\Console\Command;

use App\Command\App\Board\ApplyPriorityLabelsFromWorksheetCommand;
use App\Command\App\Board\SortCardsByPriorityCommand;
use App\Command\App\SyncLabelsToBoardCommand;
use App\Model\App\Excel\WorksheetReference;
use App\Model\App\PriorityLabel;
use App\Model\Trello\Board;
use App\Model\Trello\WantedLabel;
use App\Model\Worksheet\TaskFactory;
use App\Query\Microsoft\Drive\GetDriveItemQuery;
use App\Query\Microsoft\Drive\GetSiteDriveQuery;
use App\Query\Microsoft\Excel\FindWorksheetsQuery;
use App\Query\Microsoft\Site\FindSiteByNameQuery;
use App\Query\Trello\Board\FindBoardsByNameQuery;
use Microsoft\Graph\Model\WorkbookWorksheet;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @see https://docs.microsoft.com/en-us/graph/api/resources/excel?view=graph-rest-1.0
 */
class AppSyncComand extends Command
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $commandBus,
        private TaskFactory $taskFactory
    )
    {
        $this->messageBus = $commandBus;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:sync')
            ->setDescription('Synchronizes Trello cards to an Excel workbook stored in SharePoint')
            ->addArgument('siteName', InputArgument::OPTIONAL, 'Named site containing the workbook', 'IT')
            ->addArgument('workbookPath', InputArgument::OPTIONAL, 'Path to the workbook', '/General/IT Financial Planning.xlsx')
            ->addArgument('worksheetName', InputArgument::OPTIONAL, 'Name of the worksheet to update', 'Tasks Overview');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $worksheetReference = $this->getWorksheetReference(
            $input->getArgument('siteName'),
            $input->getArgument('workbookPath'),
            $input->getArgument('worksheetName')
        );

        /** @var Board[] $boards */
        $boards = $this->handle(new FindBoardsByNameQuery([
            'IT Projects',
            'Client Portal',
            'Linguist Portal',
            'AIMS',
            'apex-translations.com'
        ]));

        $wantedLabels = [
            new WantedLabel('Low Priority', 'green'),
            new WantedLabel('Medium Priority', 'yellow'),
            new WantedLabel('High 1 Priority', 'orange'),
            new WantedLabel('High 2 Priority', 'red'),
            new WantedLabel('High 3 Priority', 'purple'),
            new WantedLabel('Information Needed', 'pink'),
        ];

        $priorityLabels = [
            new PriorityLabel('Low', 'Low Priority'),
            new PriorityLabel('Medium', 'Medium Priority'),
            new PriorityLabel('High 3', 'High 3 Priority'),
            new PriorityLabel('High 2', 'High 2 Priority'),
            new PriorityLabel('High 1', 'High 1 Priority'),
        ];
        $labelNames = array_map(
            fn(PriorityLabel $label) => $label->getName(),
            $priorityLabels
        );

        foreach ($boards as $board) {
            $this->handle(new SyncLabelsToBoardCommand(
                $board->getId(),
                $wantedLabels
            ));

            $this->handle(new ApplyPriorityLabelsFromWorksheetCommand(
                $worksheetReference,
                $board->getId(),
                $priorityLabels
            ));

            $this->handle(new SortCardsByPriorityCommand(
                $board->getId(),
                $labelNames,
                SortCardsByPriorityCommand::DIRECTION_DESCENDING
            ));
        }

        return Command::SUCCESS;
    }

    private function getWorksheetReference(string $siteName, string $filePath, string $worksheetName)
    {
        $site = $this->handle(new FindSiteByNameQuery($siteName));
        $drive = $this->handle(new GetSiteDriveQuery($site->getId()));
        $file = $this->handle(new GetDriveItemQuery($drive->getId(), $filePath));

        /** @var WorkbookWorksheet $worksheet */
        $worksheet = $this->handle(new FindWorksheetsQuery(
            $drive->getId(),
            $file->getId(),
            $worksheetName
        ))->first();

        return new WorksheetReference(
            $drive->getId(),
            $file->getId(),
            $worksheet->getId()
        );
    }
}
