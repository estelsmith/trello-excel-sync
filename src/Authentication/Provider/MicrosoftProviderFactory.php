<?php

namespace App\Authentication\Provider;

use Stevenmaguire\OAuth2\Client\Provider\Microsoft;

class MicrosoftProviderFactory
{
    public function __construct(
        private string $tenantId,
        private string $clientId,
        private string $clientSecret
    )
    {
    }

    public function create(): Microsoft
    {
        $baseUrl = sprintf(
            'https://login.microsoftonline.com/%s',
            $this->tenantId
        );

        return new Microsoft([
            // Required
            'clientId' => $this->clientId,
            'clientSecret' => $this->clientSecret,
            // Optional
            'urlAuthorize' => sprintf('%s/oauth2/v2.0/authorize', $baseUrl),
            'urlAccessToken' => sprintf('%s/oauth2/v2.0/token', $baseUrl),
        ]);
    }
}
