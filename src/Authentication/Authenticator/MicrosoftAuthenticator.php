<?php

namespace App\Authentication\Authenticator;

use League\OAuth2\Client\Token\AccessTokenInterface;
use Stevenmaguire\OAuth2\Client\Provider\Microsoft;

class MicrosoftAuthenticator
{
    private const DEFAULT_SCOPE = 'https://graph.microsoft.com/.default';

    private ?AccessTokenInterface $accessToken;
    private string $scope = self::DEFAULT_SCOPE;

    public function __construct(
        private Microsoft $authProvider
    )
    {
    }

    public function authenticate(string $scope = self::DEFAULT_SCOPE): AccessTokenInterface
    {
        $this->scope = $scope;
        return $this->accessToken = $this->authProvider->getAccessToken('client_credentials', [
            'scope' => $scope
        ]);
    }

    public function getAccessToken(): AccessTokenInterface
    {
        $accessToken = &$this->accessToken;

        if (!$accessToken || $accessToken->hasExpired()) {
            $accessToken = $this->authenticate($this->scope);
        }

        return $accessToken;
    }
}
