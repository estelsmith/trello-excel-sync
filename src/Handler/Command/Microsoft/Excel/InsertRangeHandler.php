<?php

namespace App\Handler\Command\Microsoft\Excel;

use App\Command\Microsoft\Excel\InsertRangeCommand;
use App\Infrastructure\Microsoft\Excel\SessionManager;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\WorkbookRange;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @see https://docs.microsoft.com/en-us/graph/api/range-insert?view=graph-rest-1.0&tabs=http
 */
class InsertRangeHandler implements MessageHandlerInterface
{
    public function __construct(
        private SessionManager $sessionManager,
        private Graph $graph
    )
    {
    }

    public function __invoke(InsertRangeCommand $command)
    {
        $worksheet = $command->getWorksheetReference();

        $session = $this->sessionManager->start($worksheet->getDriveId(), $worksheet->getDriveItemId());
        $request = $this->graph->createRequest(
            'POST',
            sprintf(
                '/drives/%s/items/%s/workbook/worksheets/%s/range(address=\'%s\')/insert',
                $worksheet->getDriveId(),
                $worksheet->getDriveItemId(),
                $worksheet->getWorksheetId(),
                $command->getAddress()
            )
        );
        $request->attachBody([
            'shift' => $command->getShift()
        ]);
        $session->attachToRequest($request);

        $request->setReturnType(WorkbookRange::class);

        return $request->execute();
    }
}
