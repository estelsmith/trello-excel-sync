<?php

namespace App\Handler\Command\App\Board;

use App\Command\App\Board\ApplyPriorityLabelsFromWorksheetCommand;
use App\Command\Trello\Card\SetLabelsCommand;
use App\Model\App\Excel\WorksheetReference;
use App\Model\App\PriorityLabel;
use App\Model\Trello\Card;
use App\Model\Trello\Label;
use App\Model\Trello\TrelloList;
use App\Model\Worksheet\Task;
use App\Query\App\Excel\GetWorksheetTasksQuery;
use App\Query\Trello\Board\ListLabelsQuery;
use App\Query\Trello\Board\ListListsQuery;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Selectable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class ApplyPriorityLabelsFromWorksheetHandler implements MessageHandlerInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $queryBus
    )
    {
        $this->messageBus = $queryBus;
    }

    public function __invoke(ApplyPriorityLabelsFromWorksheetCommand $command)
    {
        $boardLabels = $this->getBoardLabels($command->getBoardId());
        $priorityLabels = $command->getPriorityLabels();
        $priorities = array_map(
            fn(PriorityLabel $label) => $label->getPriority(),
            $priorityLabels
        );
        $priorityLabelNames = array_map(
            fn(PriorityLabel $label) => $label->getName(),
            $priorityLabels
        );
        $priorityLabelMap = array_combine($priorities, $priorityLabelNames);

        $worksheetTasks = $this->getWorksheetTasks($command->getWorksheetReference());

        foreach ($this->getBoardLists($command->getBoardId()) as $list) {
            if (in_array($list->getName(), $command->getExcludeLists(), true)) {
                continue;
            }

            if ($list->hasCards()) {
                foreach ($list->getCards() as $card) {
                    $keptLabels = array_filter(
                        $card->getLabels(),
                        fn(Label $label) => !in_array($label->getName(), $priorityLabelNames)
                    );

                    /** @var Task|null $cardTask */
                    $cardTask = $worksheetTasks->filter(fn(Task $task) => $task->getTrelloCard()->getShortLink() === $card->getShortLink())->first();
                    if ($cardTask) {
                        // Card has a corresponding task entry in the spreadsheet
                        $priority = $priorityLabelMap[$cardTask->getPriority()] ?? null;
                        if ($priority) {
                            $priorityLabel = $boardLabels->filter(fn(Label $label) => $label->getName() === $priority)->first();
                            $keptLabels[] = $priorityLabel;
                        }
                    }

                    if ($card->getLabels() !== $keptLabels) {
                        // The labels we kept are different from the current labels. Update the card.
                        $labelIds = array_map(fn(Label $label) => $label->getId(), $keptLabels);
                        $this->setCardLabels($card, $labelIds);
                    }
                }
            }
        }
    }

    /**
     * @param string $boardId
     * @return iterable<Label>|Collection<Label>|Selectable<Label>
     */
    private function getBoardLabels(string $boardId)
    {
        return $this->handle(new ListLabelsQuery($boardId));
    }

    /**
     * @param string $boardId
     * @return iterable<TrelloList>|Collection<TrelloList>|Selectable<TrelloList>
     */
    private function getBoardLists(string $boardId)
    {
        return $this->handle(new ListListsQuery($boardId, 'open'));
    }

    /**
     * @param WorksheetReference $worksheet
     * @return iterable<Task>|Collection<Task>|Selectable<Task>
     */
    private function getWorksheetTasks(WorksheetReference $worksheet)
    {
        return $this->handle(new GetWorksheetTasksQuery($worksheet));
    }

    private function setCardLabels(Card $card, array $labels)
    {
        return $this->handle(new SetLabelsCommand($card->getId(), $labels));
    }
}
