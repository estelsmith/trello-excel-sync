<?php

namespace App\Handler\Command\App\Board;

use App\Command\App\Board\SortCardsByPriorityCommand;
use App\Model\Trello\Card;
use App\Model\Trello\Label;
use App\Model\Trello\TrelloList;
use App\Query\Trello\Board\ListListsQuery;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SortCardsByPriorityHandler implements MessageHandlerInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $queryBus,
        private HttpClientInterface $trelloClient
    )
    {
        $this->messageBus = $queryBus;
    }

    public function __invoke(SortCardsByPriorityCommand $command)
    {
        $lists = $this->getLists($command->getBoardId(), $command->getExcludeLists());
        $priorityLabels = $command->getLabels();
        $sortDirection = $command->getDirection();

        if ($command->getDirection() == SortCardsByPriorityCommand::DIRECTION_DESCENDING) {
            $priorityLabels = array_reverse($priorityLabels);
        }

        foreach ($lists as $list) {
            $cards = $list->getCards();
            $currentOrder = array_map(
                fn(Card $card) => $card->getId(),
                $cards
            );
            usort(
                $cards,
                function (Card $a, Card $b) use ($priorityLabels, $sortDirection) {
                    $getPriority = function (Card $card) use($priorityLabels, $sortDirection) {
                        foreach ($card->getLabels() as $label) {
                            $index = array_search($label->getName(), $priorityLabels);
                            if ($index !== false) {
                                return $index;
                            }
                        }

                        return $sortDirection === SortCardsByPriorityCommand::DIRECTION_ASCENDING ? -1 : PHP_INT_MAX;
                    };

                    $aPriority = $getPriority($a);
                    $bPriority = $getPriority($b);

                    return $aPriority <=> $bPriority;
                }
            );
            $newOrder = array_map(
                fn(Card $card) => $card->getId(),
                $cards
            );

            if ($currentOrder !== $newOrder) {
                $this->orderCards($cards);
            }
        }
    }

    /**
     * @param string $boardId
     * @param string[] $excludedLists
     * @return TrelloList[]|Collection<TrelloList>|Selectable<TrelloList>
     */
    private function getLists(string $boardId, iterable $excludedLists)
    {
        /** @var TrelloList[]|Collection<TrelloList>|Selectable<TrelloList> $lists */
        $lists = $this->handle(new ListListsQuery($boardId, ListListsQuery::CARDS_OPEN));
        return $lists->matching(
            Criteria::create()
                ->where(
                    Criteria::expr()
                        ->notIn('name', $excludedLists)
                )
        );
    }

    /**
     * @param iterable<Card> $cards
     */
    private function orderCards(iterable $cards)
    {
        $pos = 0;
        foreach ($cards as $card) {
            $response = $this->trelloClient->request(
                'PUT',
                sprintf(
                    '/1/cards/%s',
                    $card->getId()
                ),
                [
                    'json' => [
                        'pos' => ++$pos
                    ]
                ]
            );
        }
    }
}
