<?php

namespace App\Handler\Command\App;

use App\Command\App\SyncLabelsToBoardCommand;
use App\Command\Trello\Label\CreateLabelCommand;
use App\Command\Trello\Label\UpdateLabelCommand;
use App\Model\Trello\Label;
use App\Model\Trello\WantedLabel;
use App\Query\Trello\Board\ListLabelsQuery;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SyncLabelsToBoardHandler implements MessageHandlerInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $queryBus,
        private HttpClientInterface $trelloClient
    )
    {
        $this->messageBus = $this->queryBus;
    }

    public function __invoke(SyncLabelsToBoardCommand $command)
    {
        $wantedLabels = $command->getWantedLabels();
        $labels = $this->listLabels($command->getBoardId());

        foreach ($wantedLabels as $wanted) {
            $matchedLabels = $labels->matching(
                Criteria::create()
                ->where(
                    Criteria::expr()
                    ->eq('color', $wanted->getColor())
                )
            );

            if ($matchedLabels->count() > 0) {
                // We found a pre-existing label with our color. Maybe it needs renamed?
                /** @var Label $matched */
                $matched = $matchedLabels->first();
                if ($matched->getName() !== $wanted->getName()) {
                    $this->updateLabel($matched->getId(), $wanted);
                }
            } else {
                $this->createLabel($command->getBoardId(), $wanted);
            }
        }
    }

    /**
     * @param string $boardId
     * @return iterable<Label>|Collection<Label>|Selectable<Label>
     */
    private function listLabels(string $boardId)
    {
        return $this->handle(new ListLabelsQuery($boardId));
    }

    private function createLabel(string $boardId, WantedLabel $label)
    {
        return $this->handle(new CreateLabelCommand(
            $boardId,
            $label->getName(),
            $label->getColor()
        ));
    }

    private function updateLabel(string $labelId, WantedLabel $label)
    {
        return $this->handle(new UpdateLabelCommand(
            $labelId,
            $label->getName(),
            $label->getColor()
        ));
    }
}
