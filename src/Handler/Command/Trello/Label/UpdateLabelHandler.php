<?php

namespace App\Handler\Command\Trello\Label;

use App\Command\Trello\Label\UpdateLabelCommand;
use App\Model\Trello\LabelFactory;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class UpdateLabelHandler implements MessageHandlerInterface
{
    public function __construct(
        private HttpClientInterface $trelloClient,
        private LabelFactory $labelFactory
    )
    {
    }

    public function __invoke(UpdateLabelCommand $command)
    {
        $response = $this->trelloClient->request(
            'PUT',
            sprintf(
                '/1/labels/%s',
                $command->getId()
            ),
            [
                'json' => [
                    'name' => $command->getName(),
                    'color' => $command->getColor(),
                ]
            ]
        );

        return $this->labelFactory->fromApiResponse($response->toArray());
    }
}
