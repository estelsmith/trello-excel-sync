<?php

namespace App\Handler\Command\Trello\Label;

use App\Command\Trello\Label\CreateLabelCommand;
use App\Model\Trello\LabelFactory;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CreateLabelHandler implements MessageHandlerInterface
{
    public function __construct(
        private HttpClientInterface $trelloClient,
        private LabelFactory $labelFactory
    )
    {
    }

    public function __invoke(CreateLabelCommand $command)
    {
        $response = $this->trelloClient->request(
            'POST',
            '/1/labels',
            [
                'json' => [
                    'name' => $command->getName(),
                    'color' => $command->getColor(),
                    'idBoard' => $command->getBoardId()
                ]
            ]
        );

        return $this->labelFactory->fromApiResponse($response->toArray());
    }
}
