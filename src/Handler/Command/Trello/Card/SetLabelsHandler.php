<?php

namespace App\Handler\Command\Trello\Card;

use App\Command\Trello\Card\SetLabelsCommand;
use App\Model\Trello\CardFactory;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SetLabelsHandler implements MessageHandlerInterface
{
    public function __construct(
        private HttpClientInterface $trelloClient,
        private CardFactory $cardFactory
    )
    {
    }

    public function __invoke(SetLabelsCommand $command)
    {
        $response = $this->trelloClient->request(
            'PUT',
            sprintf(
                '/1/cards/%s',
                $command->getId()
            ),
            [
                'json' => [
                    'idLabels' => implode(',', $command->getLabels())
                ]
            ]
        );

        return $this->cardFactory->fromCardListItem($response->toArray());
    }
}
