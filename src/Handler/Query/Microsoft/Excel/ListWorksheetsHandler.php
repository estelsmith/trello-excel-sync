<?php

namespace App\Handler\Query\Microsoft\Excel;

use App\Infrastructure\Microsoft\Excel\SessionManager;
use App\Query\Microsoft\Excel\ListWorksheetsQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\WorkbookWorksheet;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ListWorksheetsHandler implements MessageHandlerInterface
{
    public function __construct(
        private Graph $graph,
        private SessionManager $sessionManager
    )
    {
    }

    public function __invoke(ListWorksheetsQuery $query)
    {
        $session = $this->sessionManager->start($query->getDriveId(), $query->getDriveItemId());
        $request = $this->graph->createCollectionRequest(
            'GET',
            sprintf(
                '/drives/%s/items/%s/workbook/worksheets',
                $query->getDriveId(),
                $query->getDriveItemId()
            )
        );
        $session->attachToRequest($request);

        $request->setPageSize(128);
        $request->setReturnType(WorkbookWorksheet::class);

        $items = new ArrayCollection();
        while (!$request->isEnd()) {
            foreach ($request->getPage() as $item) {
                $items->add($item);
            }
        }

        return $items;
    }
}
