<?php

namespace App\Handler\Query\Microsoft\Excel;

use App\Query\Microsoft\Excel\FindWorksheetsQuery;
use App\Query\Microsoft\Excel\ListWorksheetsQuery;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class FindWorksheetsHandler implements MessageHandlerInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $queryBus
    )
    {
        $this->messageBus = $this->queryBus;
    }

    public function __invoke(FindWorksheetsQuery $query)
    {
        return $this->listWorksheets($query)->matching(
            Criteria::create()
            ->where(
                Criteria::expr()
                ->eq(
                    'name', $query->getWorksheetName()
                )
            )
        );
    }

    private function listWorksheets(FindWorksheetsQuery $query): Selectable
    {
        return $this->handle(new ListWorksheetsQuery($query->getDriveId(), $query->getDriveItemId()));
    }
}
