<?php

namespace App\Handler\Query\Microsoft\Excel;

use App\Infrastructure\Microsoft\Excel\SessionManager;
use App\Query\Microsoft\Excel\GetRangeQuery;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\WorkbookRange;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @see https://docs.microsoft.com/en-us/graph/api/range-usedrange?view=graph-rest-1.0&tabs=http
 */
class GetRangeHandler implements MessageHandlerInterface
{
    public function __construct(
        private SessionManager $sessionManager,
        private Graph $graph
    )
    {
    }

    public function __invoke(GetRangeQuery $query)
    {
        $session = $this->sessionManager->start($query->getDriveId(), $query->getDriveItemId());
        $request = $this->graph->createRequest(
            'GET',
            sprintf(
                '/drives/%s/items/%s/workbook/worksheets/%s/range(address=\'%s\')%s',
                $query->getDriveId(),
                $query->getDriveItemId(),
                $query->getWorksheetId(),
                $query->getAddress(),
                $query->isUsedRange() ? '/usedRange' : ''
            )
        );
        $session->attachToRequest($request);

        $request->setReturnType(WorkbookRange::class);

        return $request->execute();
    }
}
