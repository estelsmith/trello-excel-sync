<?php

namespace App\Handler\Query\Microsoft\Drive;

use App\Query\Microsoft\Drive\GetSiteDriveQuery;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\Drive;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetSiteDriveHandler implements MessageHandlerInterface
{
    public function __construct(
        private Graph $graph
    )
    {
    }

    public function __invoke(GetSiteDriveQuery $query)
    {
        $request = $this->graph->createRequest(
            'GET',
            sprintf(
                '/sites/%s/drive',
                $query->getSiteId()
            )
        );

        $request->setReturnType(Drive::class);

        return $request->execute();
    }
}
