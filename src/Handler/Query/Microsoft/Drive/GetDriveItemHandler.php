<?php

namespace App\Handler\Query\Microsoft\Drive;

use App\Query\Microsoft\Drive\GetDriveItemQuery;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\DriveItem;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetDriveItemHandler implements MessageHandlerInterface
{
    public function __construct(
        private Graph $graph
    )
    {
    }

    public function __invoke(GetDriveItemQuery $query)
    {
        $request = $this->graph->createRequest(
            'GET',
            sprintf(
                '/drives/%s/root:/%s',
                $query->getDriveId(),
                $query->getPath()
            )
        );

        $request->setReturnType(DriveItem::class);

        return $request->execute();
    }
}
