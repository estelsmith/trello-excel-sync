<?php

namespace App\Handler\Query\Microsoft\Drive;

use App\Query\Microsoft\Drive\ListDriveItemsQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\DriveItem;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @see https://docs.microsoft.com/en-us/graph/api/resources/driveitem?view=graph-rest-1.0
 */
class ListDriveItemsHandler implements MessageHandlerInterface
{
    public function __construct(
        private Graph $graph
    )
    {
    }

    public function __invoke(ListDriveItemsQuery $query)
    {
        $request = $this->graph->createCollectionRequest(
            'GET',
            sprintf(
                '/drives/%s/root:/%s:/children',
                $query->getDriveId(),
                $query->getPath()
            )
        );
        $request->setPageSize(128);
        $request->setReturnType(DriveItem::class);

        $driveItems = new ArrayCollection();
        while (!$request->isEnd()) {
            foreach ($request->getPage() as $item) {
                $driveItems->add($item);
            }
        }

        return $driveItems;
    }
}
