<?php

namespace App\Handler\Query\Microsoft\Site;

use App\Query\Microsoft\ListSitesQuery;
use App\Query\Microsoft\Site\FindSiteByNameQuery;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Microsoft\Graph\Model\Site;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class FindSiteByNameHandler implements MessageHandlerInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $queryBus
    )
    {
        $this->messageBus = $this->queryBus;
    }

    public function __invoke(FindSiteByNameQuery $query)
    {
        return $this->listSites()->matching(
            Criteria::create()
                ->where(
                    Criteria::expr()
                        ->eq('name', $query->getName())
                )
        )->first() ?: null;
    }

    /**
     * @return iterable<Site>|Selectable
     */
    private function listSites()
    {
        return $this->handle(new ListSitesQuery());
    }
}
