<?php

namespace App\Handler\Query\Microsoft\Site;

use App\Query\Microsoft\Site\ListSiteListsQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\ListInfo;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ListSiteListsHandler implements MessageHandlerInterface
{
    public function __construct(
        private Graph $graph
    )
    {
        $graph->setApiVersion('beta');
    }

    public function __invoke(ListSiteListsQuery $query)
    {
        $request = $this->graph->createCollectionRequest(
            'GET',
            sprintf(
                '/sites/%s/lists',
                (string)$query->getSiteId()
            )
        );

        $request->setPageSize(128);
        $request->setReturnType(ListInfo::class);

        $lists = new ArrayCollection();
        while (!$request->isEnd()) {
            foreach ($request->getPage() as $item) {
                $lists->add($item);
            }
        }

        return $lists;
    }
}
