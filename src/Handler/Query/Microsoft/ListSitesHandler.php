<?php

namespace App\Handler\Query\Microsoft;

use App\Query\Microsoft\ListSitesQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\Site;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * @see https://docs.microsoft.com/en-us/graph/api/site-list?view=graph-rest-beta&tabs=http
 */
class ListSitesHandler implements MessageHandlerInterface
{
    public function __construct(
        private Graph $graph
    )
    {
        $graph->setApiVersion('beta');
    }

    public function __invoke(ListSitesQuery $query)
    {
        $request = $this->graph->createCollectionRequest('GET', '/sites');
        $request->setPageSize(128);
        $request->setReturnType(Site::class);

        $sites = new ArrayCollection();
        while (!$request->isEnd()) {
            foreach ($request->getPage() as $item) {
                $sites->add($item);
            }
        }

        return $sites;
    }
}
