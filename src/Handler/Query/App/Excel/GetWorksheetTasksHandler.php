<?php

namespace App\Handler\Query\App\Excel;

use App\Model\App\Excel\WorksheetReference;
use App\Model\Worksheet\TaskFactory;
use App\Query\App\Excel\GetWorksheetTasksQuery;
use App\Query\Microsoft\Excel\GetRangeQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Microsoft\Graph\Model\WorkbookRange;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class GetWorksheetTasksHandler implements MessageHandlerInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $queryBus,
        private TaskFactory $taskFactory
    )
    {
        $this->messageBus = $this->queryBus;
    }

    public function __invoke(GetWorksheetTasksQuery $query)
    {
        $headers = $this->getRange(
            $query->getWorksheetReference(),
            'A1:ZZ1'
        )->getValues()[0];
        $headers = array_map(fn($item) => strtolower($item), $headers);

        $lastColumn = chr(ord('A') + count($headers) - 1);
        $recordsRange = sprintf(
            'A2:%s9999',
            $lastColumn
        );

        /** @var array $records */
        $records = $this->getRange(
            $query->getWorksheetReference(),
            $recordsRange
        )->getValues();

        $items = new ArrayCollection();
        foreach ($records as $record) {
            $items->add($this->taskFactory->fromWorksheetRow(array_combine($headers, $record)));
        }

        return $items;
    }

    private function getRange(WorksheetReference $worksheet, string $address, bool $usedRange = GetRangeQuery::USED_RANGE): WorkbookRange
    {
        return $this->handle(new GetRangeQuery(
            $worksheet->getDriveId(),
            $worksheet->getDriveItemId(),
            $worksheet->getWorksheetId(),
            $address,
            $usedRange
        ));
    }
}
