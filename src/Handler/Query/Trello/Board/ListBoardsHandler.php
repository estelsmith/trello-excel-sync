<?php

namespace App\Handler\Query\Trello\Board;

use App\Model\Trello\BoardFactory;
use App\Query\Trello\Board\ListBoardsQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @see https://developer.atlassian.com/cloud/trello/rest/api-group-members/#api-members-id-boards-get
 */
class ListBoardsHandler implements MessageHandlerInterface
{
    public function __construct(
        private HttpClientInterface $trelloClient,
        private BoardFactory $boardFactory
    )
    {
    }

    public function __invoke(ListBoardsQuery $query)
    {
        $response = $this->trelloClient->request(
            'GET',
            '/1/members/me/boards',
            [
                'query' => [
                    'filter' => $query->getFilter()
                ]
            ]
        );

        $boards = new ArrayCollection();
        foreach ($response->toArray() as $board) {
            $boards->add($this->boardFactory->fromApiResponse($board));
        }

        return $boards;
    }
}
