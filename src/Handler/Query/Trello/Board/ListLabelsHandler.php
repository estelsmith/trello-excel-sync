<?php

namespace App\Handler\Query\Trello\Board;

use App\Model\Trello\LabelFactory;
use App\Query\Trello\Board\ListLabelsQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ListLabelsHandler implements MessageHandlerInterface
{
    public function __construct(
        private HttpClientInterface $trelloClient,
        private LabelFactory $labelFactory
    )
    {
    }

    public function __invoke(ListLabelsQuery $query)
    {
        $response = $this->trelloClient->request(
            'GET',
            sprintf(
                '/1/boards/%s/labels',
                $query->getBoardId()
            ),
            [
                'query' => [
                    'limit' => 1000
                ]
            ]
        );

        $labels = new ArrayCollection();
        foreach ($response->toArray() as $label) {
            $labels->add($this->labelFactory->fromApiResponse($label));
        }

        return $labels;
    }
}
