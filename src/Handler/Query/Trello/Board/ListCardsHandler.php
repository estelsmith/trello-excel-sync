<?php

namespace App\Handler\Query\Trello\Board;

use App\Model\Trello\CardFactory;
use App\Query\Trello\Board\ListCardsQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ListCardsHandler implements MessageHandlerInterface
{
    public function __construct(
        private HttpClientInterface $trelloClient,
        private CardFactory $cardFactory
    )
    {
    }

    public function __invoke(ListCardsQuery $query)
    {
        $response = $this->trelloClient->request(
            'GET',
            sprintf(
                '/1/boards/%s/cards/%s',
                $query->getBoardId(),
                $query->getFilter()
            ),
            [
                'query' => [
                    'filter' => $query->getFilter()
                ]
            ]
        );

        $cards = new ArrayCollection();
        foreach ($response->toArray() as $item) {
            $cards->add($this->cardFactory->fromCardListItem($item));
        }

        return $cards;
    }
}
