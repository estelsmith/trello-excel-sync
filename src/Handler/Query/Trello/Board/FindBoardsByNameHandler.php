<?php

namespace App\Handler\Query\Trello\Board;

use App\Model\Trello\Board;
use App\Query\Trello\Board\FindBoardsByNameQuery;
use App\Query\Trello\Board\ListBoardsQuery;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Selectable;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;

class FindBoardsByNameHandler implements MessageHandlerInterface
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $queryBus
    )
    {
        $this->messageBus = $this->queryBus;
    }

    public function __invoke(FindBoardsByNameQuery $query)
    {
        return $this->listBoards()->matching(
            Criteria::create()
                ->where(
                    Criteria::expr()
                        ->in('name', $query->getNames())
                )
        );
    }

    /**
     * @return Board[]|Collection<Board>|Selectable<Board>
     */
    private function listBoards()
    {
        return $this->handle(new ListBoardsQuery('open'));
    }
}
