<?php

namespace App\Handler\Query\Trello\Board;

use App\Model\Trello\TrelloListFactory;
use App\Query\Trello\Board\ListListsQuery;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ListListsHandler implements MessageHandlerInterface
{
    public function __construct(
        private HttpClientInterface $trelloClient,
        private TrelloListFactory $listFactory
    )
    {
    }

    public function __invoke(ListListsQuery $query)
    {
        $response = $this->trelloClient->request(
            'GET',
            sprintf(
                '/1/boards/%s/lists',
                $query->getBoardId()
            ),
            [
                'query' => [
                    'cards' => $query->getCards(),
                    'filter' => 'open'
                ]
            ]
        );

        $items = new ArrayCollection();
        foreach ($response->toArray() as $item) {
            $items->add($this->listFactory->fromApiListResponse($item));
        }

        return $items;
    }
}
