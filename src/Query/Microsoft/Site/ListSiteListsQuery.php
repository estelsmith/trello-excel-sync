<?php

namespace App\Query\Microsoft\Site;

use App\Domain\Microsoft\Site\Id;
use App\QueryInterface;

class ListSiteListsQuery implements QueryInterface
{
    public function __construct(
        private Id $siteId
    )
    {
    }

    public function getSiteId(): Id
    {
        return $this->siteId;
    }
}
