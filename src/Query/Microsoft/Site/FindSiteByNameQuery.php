<?php

namespace App\Query\Microsoft\Site;

use App\QueryInterface;

class FindSiteByNameQuery implements QueryInterface
{
    public function __construct(
        private string $name
    )
    {
    }

    public function getName(): string
    {
        return $this->name;
    }
}
