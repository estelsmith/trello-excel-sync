<?php

namespace App\Query\Microsoft\Drive;

use App\QueryInterface;

class GetDriveItemQuery implements QueryInterface
{
    public function __construct(
        private string $driveId,
        private string $path
    )
    {
    }

    public function getDriveId(): string
    {
        return $this->driveId;
    }

    public function getPath(): string
    {
        return $this->path;
    }
}
