<?php

namespace App\Query\Microsoft\Drive;

use App\QueryInterface;

class GetSiteDriveQuery implements QueryInterface
{
    public function __construct(
        private string $siteId
    )
    {
    }

    public function getSiteId(): string
    {
        return $this->siteId;
    }
}
