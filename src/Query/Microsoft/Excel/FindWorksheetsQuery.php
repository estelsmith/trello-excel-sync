<?php

namespace App\Query\Microsoft\Excel;

use App\QueryInterface;

class FindWorksheetsQuery implements QueryInterface
{
    public function __construct(
        private string $driveId,
        private string $driveItemId,
        private string $worksheetName
    )
    {
    }

    public function getDriveId(): string
    {
        return $this->driveId;
    }

    public function getDriveItemId(): string
    {
        return $this->driveItemId;
    }

    public function getWorksheetName(): string
    {
        return $this->worksheetName;
    }
}
