<?php

namespace App\Query\Microsoft\Excel;

use App\QueryInterface;

class GetRangeQuery implements QueryInterface
{
    public const USED_RANGE = true;
    public const NO_USED_RANGE = false;

    public function __construct(
        private string $driveId,
        private string $driveItemId,
        private string $worksheetId,
        private string $address,
        private bool $usedRange = self::USED_RANGE
    )
    {
    }

    public function getDriveId(): string
    {
        return $this->driveId;
    }

    public function getDriveItemId(): string
    {
        return $this->driveItemId;
    }

    public function getWorksheetId(): string
    {
        return $this->worksheetId;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function isUsedRange(): bool
    {
        return $this->usedRange;
    }
}
