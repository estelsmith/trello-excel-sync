<?php

namespace App\Query\Microsoft\Excel;

use App\QueryInterface;

class ListWorksheetsQuery implements QueryInterface
{
    public function __construct(
        private string $driveId,
        private string $driveItemId
    )
    {
    }

    public function getDriveId(): string
    {
        return $this->driveId;
    }

    public function getDriveItemId(): string
    {
        return $this->driveItemId;
    }
}
