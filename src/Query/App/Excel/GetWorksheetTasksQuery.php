<?php

namespace App\Query\App\Excel;

use App\Model\App\Excel\WorksheetReference;
use App\QueryInterface;

class GetWorksheetTasksQuery implements QueryInterface
{
    public function __construct(
        private WorksheetReference $worksheetReference
    )
    {
    }

    public function getWorksheetReference(): WorksheetReference
    {
        return $this->worksheetReference;
    }
}
