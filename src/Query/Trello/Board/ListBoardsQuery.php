<?php

namespace App\Query\Trello\Board;

use App\QueryInterface;

/**
 * @see https://developer.atlassian.com/cloud/trello/rest/api-group-members/#api-members-id-boards-get
 */
class ListBoardsQuery implements QueryInterface
{
    public function __construct(
        private string $filter = 'open'
    )
    {
    }

    public function getFilter(): string
    {
        return $this->filter;
    }
}
