<?php

namespace App\Query\Trello\Board;

use App\QueryInterface;

/**
 * @see https://developer.atlassian.com/cloud/trello/rest/api-group-boards/#api-boards-id-lists-get
 */
class ListListsQuery implements QueryInterface
{
    public const CARDS_NONE = 'none';
    public const CARDS_ALL = 'all';
    public const CARDS_CLOSED = 'closed';
    public const CARDS_OPEN = 'open';

    public function __construct(
        private string $boardId,
        private string $cards = self::CARDS_NONE
    )
    {
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    public function getCards(): string
    {
        return $this->cards;
    }
}
