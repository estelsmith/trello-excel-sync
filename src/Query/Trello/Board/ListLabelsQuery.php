<?php

namespace App\Query\Trello\Board;

use App\QueryInterface;

class ListLabelsQuery implements QueryInterface
{
    public function __construct(
        private string $boardId
    )
    {
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }
}
