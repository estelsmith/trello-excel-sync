<?php

namespace App\Query\Trello\Board;

use App\QueryInterface;

class FindBoardsByNameQuery implements QueryInterface
{
    /**
     * @param iterable<string> $names
     */
    public function __construct(
        private iterable $names
    )
    {
    }

    /**
     * @return iterable<string>
     */
    public function getNames(): iterable
    {
        return $this->names;
    }
}
