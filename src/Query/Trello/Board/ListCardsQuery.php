<?php

namespace App\Query\Trello\Board;

use App\QueryInterface;

class ListCardsQuery implements QueryInterface
{
    public function __construct(
        private string $boardId,
        private string $filter = 'visible'
    )
    {
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    public function getFilter(): string
    {
        return $this->filter;
    }
}
